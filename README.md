# Huge Inc's React Native League App - Stuckoverflow

The repository includes 3 packages in a yarn workspace (monorepo):

    /packages
        /components (shared react native components)
        /app (react native app)
        /web (react native web)

## Components

In `/packages/components` you'll find react native components that will be shared in the mobile app (built with react native) and the web version (built with react native web).

## Prerequisites

Install yarn classic (latest v1). React native doesn't support v2 yet.

## Installation

Run `yarn` in the root of the repository. Yarn will treat the 3 packages as part of a single repo (despite having separate package.json and living in separate folders) and install dependencies for the 3 of them.

## Web App Usage

The Web App is built with Next.js.

To start it in dev mode from `/packages/web` run `yarn dev`.

## React Native Usage

Follow the instructions for [the env setup](https://reactnative.dev/docs/environment-setup) with the React Native CLI (not expo).

To start the app in IOS, from `/packages/app` run `yarn react-native run-ios`.
To start the app in Android, from `/packages/app` run `yarn react-native run-android`.

## Autoformatting and Editor Setup

The project has a VS Code configuration in ./vscode that enables autoformatting files automatically when you save based on Prettier and ESLint rules.

For the autoformatting to work in VS Code make sure that [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) and [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) extensions are installed. 